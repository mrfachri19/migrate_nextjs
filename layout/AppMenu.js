import React, { useContext } from "react";
import AppMenuitem from "./AppMenuitem";
import { LayoutContext } from "./context/layoutcontext";
import { MenuProvider } from "./context/menucontext";
import Link from "next/link";
import PrivateRoute from "../pages/utilities/PrivateRoute";

const AppMenu = () => {
  const { layoutConfig } = useContext(LayoutContext);

  const model = [
    {
      label: "Home",
      items: [{ label: "Dashboard", icon: "pi pi-fw pi-home", to: "/uikit/dashboard" }],
    },
    {
      label: "Master Data",
      items: [
        { label: "Country", icon: "pi pi-fw pi-id-card", to: "/uikit/country" },
        { label: "Prev", icon: "pi pi-fw pi-check-square", to: "/uikit/prev" },
        { label: "City", icon: "pi pi-fw pi-bookmark", to: "/uikit/city" },
        {
          label: "Contact Category",
          icon: "pi pi-fw pi-exclamation-circle",
          to: "/uikit/contact",
        },
        {
          label: "Contact",
          icon: "pi pi-fw pi-mobile",
          to: "/uikit/contact_user",
          class: "rotated-icon",
        },
        { label: "User", icon: "pi pi-fw pi-table", to: "/uikit/user" },
        { label: "Role", icon: "pi pi-fw pi-list", to: "/uikit/role" },
        {
          label: "User Role",
          icon: "pi pi-fw pi-share-alt",
          to: "/uikit/user_role",
        },
        {
          label: "Role Detail Master",
          icon: "pi pi-fw pi-tablet",
          to: "/uikit/role_detail_master",
        },
        {
          label: "Role Detail",
          icon: "pi pi-fw pi-clone",
          to: "/uikit/role_detail",
        },
        { label: "Area", icon: "pi pi-fw pi-image", to: "/uikit/area" },
        { label: "Branch", icon: "pi pi-fw pi-bars", to: "/uikit/branch" },
        {
          label: "Location",
          icon: "pi pi-fw pi-comment",
          to: "/uikit/location",
        },
        { label: "Division", icon: "pi pi-fw pi-file", to: "/uikit/division" },
        {
          label: "Sub Division",
          icon: "pi pi-fw pi-chart-bar",
          to: "/uikit/sub_division",
        },
        {
          label: "Warehouse",
          icon: "pi pi-fw pi-circle",
          to: "/uikit/warehouse",
        },
      ],
    },
    // {
    //     label: 'Prime Blocks',
    //     items: [
    //         { label: 'Free Blocks', icon: 'pi pi-fw pi-eye', to: '/blocks', badge: 'NEW' },
    //         { label: 'All Blocks', icon: 'pi pi-fw pi-globe', url: 'https://blocks.primereact.org', target: '_blank' }
    //     ]
    // },
    // {
    //     label: 'Utilities',
    //     items: [
    //         { label: 'PrimeIcons', icon: 'pi pi-fw pi-prime', to: '/utilities/icons' },
    //         { label: 'PrimeFlex', icon: 'pi pi-fw pi-desktop', url: 'https://www.primefaces.org/primeflex/', target: '_blank' }
    //     ]
    // },
    // {
    //     label: 'Pages',
    //     icon: 'pi pi-fw pi-briefcase',
    //     to: '/pages',
    //     items: [
    //         {
    //             label: 'Landing',
    //             icon: 'pi pi-fw pi-globe',
    //             to: '/landing'
    //         },
    //         {
    //             label: 'Auth',
    //             icon: 'pi pi-fw pi-user',
    //             items: [
    //                 {
    //                     label: 'Login',
    //                     icon: 'pi pi-fw pi-sign-in',
    //                     to: '/auth/login'
    //                 },
    //                 {
    //                     label: 'Error',
    //                     icon: 'pi pi-fw pi-times-circle',
    //                     to: '/auth/error'
    //                 },
    //                 {
    //                     label: 'Access Denied',
    //                     icon: 'pi pi-fw pi-lock',
    //                     to: '/auth/access'
    //                 }
    //             ]
    //         },
    //         {
    //             label: 'Crud',
    //             icon: 'pi pi-fw pi-pencil',
    //             to: '/pages/crud'
    //         },
    //         {
    //             label: 'Timeline',
    //             icon: 'pi pi-fw pi-calendar',
    //             to: '/pages/timeline'
    //         },
    //         {
    //             label: 'Not Found',
    //             icon: 'pi pi-fw pi-exclamation-circle',
    //             to: '/pages/notfound'
    //         },
    //         {
    //             label: 'Empty',
    //             icon: 'pi pi-fw pi-circle-off',
    //             to: '/pages/empty'
    //         }
    //     ]
    // },
    // {
    //     label: 'Hierarchy',
    //     items: [
    //         {
    //             label: 'Submenu 1',
    //             icon: 'pi pi-fw pi-bookmark',
    //             items: [
    //                 {
    //                     label: 'Submenu 1.1',
    //                     icon: 'pi pi-fw pi-bookmark',
    //                     items: [
    //                         { label: 'Submenu 1.1.1', icon: 'pi pi-fw pi-bookmark' },
    //                         { label: 'Submenu 1.1.2', icon: 'pi pi-fw pi-bookmark' },
    //                         { label: 'Submenu 1.1.3', icon: 'pi pi-fw pi-bookmark' }
    //                     ]
    //                 },
    //                 {
    //                     label: 'Submenu 1.2',
    //                     icon: 'pi pi-fw pi-bookmark',
    //                     items: [{ label: 'Submenu 1.2.1', icon: 'pi pi-fw pi-bookmark' }]
    //                 }
    //             ]
    //         },
    //         {
    //             label: 'Submenu 2',
    //             icon: 'pi pi-fw pi-bookmark',
    //             items: [
    //                 {
    //                     label: 'Submenu 2.1',
    //                     icon: 'pi pi-fw pi-bookmark',
    //                     items: [
    //                         { label: 'Submenu 2.1.1', icon: 'pi pi-fw pi-bookmark' },
    //                         { label: 'Submenu 2.1.2', icon: 'pi pi-fw pi-bookmark' }
    //                     ]
    //                 },
    //                 {
    //                     label: 'Submenu 2.2',
    //                     icon: 'pi pi-fw pi-bookmark',
    //                     items: [{ label: 'Submenu 2.2.1', icon: 'pi pi-fw pi-bookmark' }]
    //                 }
    //             ]
    //         }
    //     ]
    // },
    // {
    //     label: 'Get Started',
    //     items: [
    //         {
    //             label: 'Documentation',
    //             icon: 'pi pi-fw pi-question',
    //             to: '/documentation'
    //         },
    //         {
    //             label: 'View Source',
    //             icon: 'pi pi-fw pi-search',
    //             url: 'https://github.com/primefaces/sakai-react',
    //             target: '_blank'
    //         }
    //     ]
    // }
  ];

  return (
    <MenuProvider>
      <ul className="layout-menu">
          {model.map((item, i) => {
            return !item.seperator ? (
              <AppMenuitem item={item} root={true} index={i} key={item.label} />
            ) : (
              <li className="menu-separator"></li>
            );
          })}
      </ul>
    </MenuProvider>
  );
};

export default AppMenu;
