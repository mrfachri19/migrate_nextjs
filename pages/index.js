import React, { useContext, useRef, useState } from "react";
import Link from "next/link";

import { StyleClass } from "primereact/styleclass";
import { Button } from "primereact/button";
import { Ripple } from "primereact/ripple";
import { Divider } from "primereact/divider";
import AppConfig from "../layout/AppConfig";
import { LayoutContext } from "../layout/context/layoutcontext";
import { classNames } from "primereact/utils";
import LoginPage from "./auth/login";

const LandingPage = () => {
  const [isHidden, setIsHidden] = useState(false);
  const { layoutConfig } = useContext(LayoutContext);
  const menuRef = useRef();

  const toggleMenuItemClick = () => {
    setIsHidden((prevState) => !prevState);
  };

  return (
   <>
   <LoginPage />
   </>
  );
};

LandingPage.getLayout = function getLayout(page) {
  return (
    <React.Fragment>
      {page}
      <AppConfig simple />
    </React.Fragment>
  );
};

export default LandingPage;
