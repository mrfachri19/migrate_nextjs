// run "npm test" or "npm start" for development
// --------------------or-----------------------
// simply run "npm run build" for production

const APP = {
  API_URL: "https://wms.onesolution.business/geta_wms_be"
};

export default APP;
