import React, { useState, useEffect } from "react";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
// api
import { postData, getData } from "../../api";
import { Sidebar } from "primereact/sidebar";
import { Messaege } from "../../utilities/helper";
import { ConfirmPopup, confirmPopup } from "primereact/confirmpopup";
import { useRef } from "react";
import { Toast } from "primereact/toast";
import { Dropdown } from "primereact/dropdown";

const RoleDetailMaster = () => {
  const [filters1, setFilters1] = useState(null);
  const [loading1, setLoading1] = useState(true);
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [dataCountry, setDataCountry] = useState([]);
  const [visibleRight, setVisibleRight] = useState(false);
  const [detail, setDetail] = useState({});
  const [contryCode, setCountryCode] = useState("");
  const [valueMaster, setValueMaster ] = useState("");
  const [masterDescription, setMasterDescription] = useState("");
  const [show, setShow] = useState("");
  const toast = useRef(null);
  const [userRoledata, setUserRoledata] = useState([]);
  const [userRole, setUserRole] = useState("");
  const [roledata, setRoledata] = useState([]);
  const [role, setRole] = useState("");

  const onGlobalFilterChange1 = (e) => {
    const value = e.target.value;
    let _filters1 = { ...filters1 };
    _filters1["global"].value = value;

    setFilters1(_filters1);
    setGlobalFilterValue1(value);
  };

  const renderHeader1 = () => {
    return (
      <div className="flex justify-content-between">
        <Button
          type="button"
          // icon="pi pi-filter-slash"
          label="Add Data"
          outlined
          onClick={() => {
            setVisibleRight(true);
            setShow("post");
          }}
        />
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue1}
            onChange={onGlobalFilterChange1}
            placeholder="Keyword Search"
          />
        </span>
      </div>
    );
  };

  const roledetailmasterData = async () => {
    try {
      const response = await getData("/roledetailmaster/read?_=1686045944126");
      var temp = [];
      temp = response.data.data.original.data;
      console.log(temp);
      setDataCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    roledetailmasterData();
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      name: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      "role_detail_master_name": {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      representative: { value: null, matchMode: FilterMatchMode.IN },
      date: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.DATE_IS }],
      },
      balance: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      status: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      activity: { value: null, matchMode: FilterMatchMode.BETWEEN },
      verified: { value: null, matchMode: FilterMatchMode.EQUALS },
    });
    setGlobalFilterValue1("");
  };

  const roledetailmasterBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.role_detail_master_code}
        </span>
      </React.Fragment>
    );
  };

  const nameBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.role_detail_master_name}
        </span>
      </React.Fragment>
    );
  };

  const rolecodebodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.role_code}
        </span>
      </React.Fragment>
    );
  };

  const filterClearTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-times"
        onClick={options.filterClearCallback}
        severity="secondary"
      ></Button>
    );
  };

  const filterApplyTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-check"
        onClick={options.filterApplyCallback}
        severity="success"
      ></Button>
    );
  };

  const getroledetailmasterData = async (code) => {
    try {
      const response = await getData(
        `/roledetailmaster/datamodal?role_detail_master_code=${code}`
      );
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDetail(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const SubmitCountryData = async () => {
    try {
      const response = await postData("/roledetailmaster/submit", {
        role_detail_master_name: contryCode,
        role_detail_master_code: userRole,
        role_detail_master_desc: role,
        value: valueMaster,
        role_code: role,
        user: "1",
      });
      console.log(response);
      if(response.data.code == 300) {
        Messaege('Failed', 'Make sure everything is filled', 'error');
      } else {
        Messaege("Succes", "Succes add data", "success");
      }      setVisibleRight(false);
      roledetailmasterData();
    } catch (error) {
      console.log(error);
    }
  };

  const editCountryData = async () => {
    try {
      const response = await postData("/roledetailmaster/save", {
        role_detail_master_name: contryCode ? contryCode : detail.role_detail_master_name,
        role_detail_master_code: userRole ? userRole : detail.role_detail_master_code,
        role_detail_master_desc: role ? role : detail.role_detail_master_desc,
        value: valueMaster ? valueMaster : detail.value,
        role_code: role ? role : detail.role_code,
        user: detail.user ? detail.user : "1",
      });
      console.log(response);
      if(response.data.code == 300) {
        Messaege('Failed', 'Make sure everything is filled', 'error');
      } else {
        Messaege("Succes", "Succes add data", "success");
      }      setVisibleRight(false);
      roledetailmasterData();
    } catch (error) {
      console.log(error);
    }
  };

  const verifiedBodyTemplate = (rowData) => {
    const delroledetailmasterData = async () => {
      try {
        const response = await postData("/roledetailmaster/delete", {
          role_detail_master_name: rowData.role_detail_master_name,
          role_detail_master_code: rowData.role_detail_master_code,
          role_detail_master_desc: rowData.role_detail_master_desc,
          value: rowData.value,
          role_code: rowData.role_code,
          user: rowData.user ? rowData.user : "1",
        });
        console.log(response);
        Messaege("Succes", "Succes delete data", "success");
        roledetailmasterData();
      } catch (error) {
        console.log(error);
      }
    };
    const accept = () => {
      toast.current.show({
        severity: "info",
        summary: "Confirmed",
        detail: "You have accepted",
        life: 3000,
      });
      delroledetailmasterData();
    };

    const reject = () => {
      toast.current.show({
        severity: "error",
        summary: "Rejected",
        detail: "You have rejected",
        life: 3000,
      });
    };

    const confirm = (event) => {
      confirmPopup({
        target: event.currentTarget,
        message: "Are you sure you want to delete data?",
        icon: "pi pi-exclamation-triangle",
        accept,
        reject,
      });
    };

    return (
      <>
        <div className="flex mx-3 gap-5">
          <Button
            type="button"
            icon="pi text-white pi-book"
            severity="success"
            onClick={() => {
              setVisibleRight(true);
              getroledetailmasterData(rowData.role_detail_master_code);
              setShow("read");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            type="button"
            icon="pi text-white pi-pencil"
            severity="warning"
            onClick={() => {
              setVisibleRight(true), setShow("update");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            onClick={confirm}
            type="button"
            icon="pi text-white pi-trash"
            severity="danger"
            style={{ marginRight: ".25em" }}
          ></Button>
        </div>
      </>
    );
  };

  const header1 = renderHeader1();

  const getRole = async () => {
    try {
      const response = await getData(`/roledetailmaster/roleloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setRoledata(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getRole();
  }, []);


  const items2 = roledata?.map((item) => {
    const data = {};
    data.name = item.role_name;
    data.value = item.role_code;
    return data;
  });

  return (
    <div className="grid">
      <Toast ref={toast} />
      <ConfirmPopup />
      <div className="col-12">
        <div className="card">
          <h5>Filter Menu</h5>
          <DataTable
            value={dataCountry}
            paginator
            className="p-datatable-gridlines"
            showGridlines
            rows={10}
            dataKey="id"
            filters={filters1}
            filterDisplay="menu"
            loading={loading1}
            responsiveLayout="scroll"
            emptyMessage="No customers found."
            header={header1}
          >
            <Column
              header="Master Code"
              filterField="roledetailmaster"
              style={{ minWidth: "12rem" }}
              body={roledetailmasterBodyTemplate}
              filter
              filterPlaceholder="Search by roledetailmaster"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Master Name"
              filterField="role_detail_master_name"
              style={{ minWidth: "12rem" }}
              body={nameBodyTemplate}
              filter
              filterPlaceholder="Search by roledetailmaster"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Role Code"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={rolecodebodyTemplate}
              filter
              filterPlaceholder="Search by roledetailmaster"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Action"
              style={{ minWidth: "12rem" }}
              body={verifiedBodyTemplate}
            />
          </DataTable>
          <Sidebar
            visible={visibleRight}
            onHide={() => setVisibleRight(false)}
            baseZIndex={1000}
            position="right"
          >
            <h5 style={{ fontWeight: "normal" }}>
              Role Detail Master Information
            </h5>
            <div className="mt-5 p-fluid">
              <div className="field">
                <label htmlFor="code">Master Code</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.role_detail_master_code}
                  onChange={(e) => setCountryCode(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Master Name</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.role_detail_master_name}
                  onChange={(e) => setUserRoledata(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Role Code</label>
                <Dropdown
                  value={role}
                  onChange={(e) => setRole(e.value)}
                  options={items2}
                  optionLabel="name"
                  placeholder={show == "post" ? "" : detail.role_code}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Value</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.value}
                  onChange={(e) => setValueMaster(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Master Description</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.role_detail_master_desc}
                  onChange={(e) => setMasterDescription(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              {show == "update" || show == "post" ? (
                <Button
                  className="mt-5"
                  label="Save"
                  severity="Info"
                  onClick={show == "post" ? SubmitCountryData : editCountryData}
                />
              ) : (
                <></>
              )}
            </div>
          </Sidebar>
        </div>
      </div>
    </div>
  );
};

export default RoleDetailMaster;
