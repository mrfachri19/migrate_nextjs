import React, { useState, useEffect } from "react";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
// api
import { postData, getData } from "../../api";
import { Sidebar } from "primereact/sidebar";
import { Messaege } from "../../utilities/helper";
import { ConfirmPopup, confirmPopup } from "primereact/confirmpopup";
import { useRef } from "react";
import { Toast } from "primereact/toast";
import { Dropdown } from "primereact/dropdown";
import { InputTextarea } from "primereact/inputtextarea";

const Location = () => {
  const [filters1, setFilters1] = useState(null);
  const [loading1, setLoading1] = useState(true);
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [dataCountry, setDataCountry] = useState([]);
  const [visibleRight, setVisibleRight] = useState(false);
  const [detail, setDetail] = useState({});
  const [dataCodeContry, setDataCodeCountry] = useState([]);
  const [show, setShow] = useState("");
  const toast = useRef(null);
  const [locationCode1, setLocationCode1] = useState("");
  const [locationname, setlocationname] = useState("");
  const [locationdesc, setlocationdesc] = useState("");
  const [locationadress, setlocationadress] = useState("");
  const [citycode, setcitycode] = useState("");
  const [postcode, setpostcode] = useState("");
  const [phonenumber, setphonenumber] = useState("");
  const [branchCode, setBranchCode] = useState("");
  const [dataBranchcode, setdataBranchCode] = useState([]);

  const onGlobalFilterChange1 = (e) => {
    const value = e.target.value;
    let _filters1 = { ...filters1 };
    _filters1["global"].value = value;

    setFilters1(_filters1);
    setGlobalFilterValue1(value);
  };

  const renderHeader1 = () => {
    return (
      <div className="flex justify-content-between">
        <Button
          type="button"
          // icon="pi pi-filter-slash"
          label="Add Data"
          outlined
          onClick={() => {
            setVisibleRight(true);
            setShow("post");
          }}
        />
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue1}
            onChange={onGlobalFilterChange1}
            placeholder="Keyword Search"
          />
        </span>
      </div>
    );
  };

  const locationData = async () => {
    try {
      const response = await getData("/location/read?_=1686045944126");
      var temp = [];
      temp = response.data.data.original.data;
      console.log(temp);
      setDataCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    locationData();
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      name: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      "location_name": {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      representative: { value: null, matchMode: FilterMatchMode.IN },
      date: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.DATE_IS }],
      },
      balance: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      status: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      activity: { value: null, matchMode: FilterMatchMode.BETWEEN },
      verified: { value: null, matchMode: FilterMatchMode.EQUALS },
    });
    setGlobalFilterValue1("");
  };

  const locationBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.location_code}
        </span>
      </React.Fragment>
    );
  };

  const nameBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.location_name}
        </span>
      </React.Fragment>
    );
  };

  const Branchcodetemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.branch_code}
        </span>
      </React.Fragment>
    );
  };

  const filterClearTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-times"
        onClick={options.filterClearCallback}
        severity="secondary"
      ></Button>
    );
  };

  const filterApplyTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-check"
        onClick={options.filterApplyCallback}
        severity="success"
      ></Button>
    );
  };

  const getlocationData = async (code) => {
    try {
      const response = await getData(
        `/location/datamodal?location_code=${code}`
      );
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDetail(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const SubmitCountryData = async () => {
    try {
      const response = await postData("/location/submit", {
        location_code: locationCode1,
        location_name: locationname,
        location_desc: locationdesc,
        location_address: locationadress,
        branch_code: branchCode,
        city_code: citycode,
        post_code: postcode,
        phone_number: phonenumber,
        user: "1",
      });
      console.log(response);
      if(response.data.code == 300) {
        Messaege('Failed', 'Make sure everything is filled', 'error');
      } else {
        Messaege("Succes", "Succes add data", "success");
      }      setVisibleRight(false);
      locationData();
    } catch (error) {
      console.log(error);
    }
  };

  const editCountryData = async () => {
    try {
      const response = await postData("/location/save", {
        location_code: locationCode1 ? locationCode1 : detail.location_code,
        location_name: locationname ? locationname : detail.location_name,
        location_desc: locationdesc ? locationdesc : detail.location_desc,
        location_address: locationadress
          ? locationadress
          : detail.location_address,
        branch_code: branchCode ? branchCode : detail.branch_code,
        city_code: citycode ? citycode : detail.city_code,
        post_code: postcode ? postcode : detail.post_code,
        phone_number: phonenumber ? phonenumber : detail.phone_number,
        user: detail.user ? detail.user : "1",
      });
      console.log(response);
      if(response.data.code == 300) {
        Messaege('Failed', 'Make sure everything is filled', 'error');
      } else {
        Messaege("Succes", "Succes add data", "success");
      }      setVisibleRight(false);
      locationData();
    } catch (error) {
      console.log(error);
    }
  };

  const verifiedBodyTemplate = (rowData) => {
    const dellocationData = async () => {
      try {
        const response = await postData("/location/delete", {
          location_code: rowData.location_code,
          location_name: rowData.location_name,
          location_desc: rowData.location_desc,
          location_address: rowData.location_address,
          branch_code: rowData.branch_code,
          city_code: rowData.city_code,
          post_code: rowData.post_code,
          phone_number: rowData.phone_number,
          user: rowData.user ? rowData.user : "1",
        });
        console.log(response);
        Messaege("Succes", "Succes delete data", "success");
        locationData();
      } catch (error) {
        console.log(error);
      }
    };
    const accept = () => {
      toast.current.show({
        severity: "info",
        summary: "Confirmed",
        detail: "You have accepted",
        life: 3000,
      });
      dellocationData();
    };

    const reject = () => {
      toast.current.show({
        severity: "error",
        summary: "Rejected",
        detail: "You have rejected",
        life: 3000,
      });
    };

    const confirm = (event) => {
      confirmPopup({
        target: event.currentTarget,
        message: "Are you sure you want to delete data?",
        icon: "pi pi-exclamation-triangle",
        accept,
        reject,
      });
    };

    return (
      <>
        <div className="flex mx-3 gap-5">
          <Button
            type="button"
            icon="pi text-white pi-book"
            severity="success"
            onClick={() => {
              setVisibleRight(true);
              getlocationData(rowData.location_code);
              setShow("read");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            type="button"
            icon="pi text-white pi-pencil"
            severity="warning"
            onClick={() => {
              setVisibleRight(true), setShow("update");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            onClick={confirm}
            type="button"
            icon="pi text-white pi-trash"
            severity="danger"
            style={{ marginRight: ".25em" }}
          ></Button>
        </div>
      </>
    );
  };

  const header1 = renderHeader1();

  const getlocationDatacountry = async (code) => {
    try {
      const response = await getData(`/location/cityloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataCodeCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getBranchCode = async (code) => {
    try {
      const response = await getData(`/location/branchloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setdataBranchCode(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getlocationDatacountry();
    getBranchCode();
  }, []);

  const items = dataCodeContry?.map((item) => {
    const data = {};
    data.name = item.city_name;
    data.value = item.city_code;
    return data;
  });

  const items2 = dataBranchcode?.map((item) => {
    const data = {};
    data.name = item.branch_name;
    data.value = item.branch_code;
    return data;
  });

  return (
    <div className="grid">
      <Toast ref={toast} />
      <ConfirmPopup />
      <div className="col-12">
        <div className="card">
          <h5>Filter Menu</h5>
          <DataTable
            value={dataCountry}
            paginator
            className="p-datatable-gridlines"
            showGridlines
            rows={10}
            dataKey="id"
            filters={filters1}
            filterDisplay="menu"
            loading={loading1}
            responsiveLayout="scroll"
            emptyMessage="No customers found."
            header={header1}
          >
            <Column
              header="Location Code"
              filterField="location"
              style={{ minWidth: "12rem" }}
              body={locationBodyTemplate}
              filter
              filterPlaceholder="Search by location"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Location Name"
              filterField="location_name"
              style={{ minWidth: "12rem" }}
              body={nameBodyTemplate}
              filter
              filterPlaceholder="Search by location"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Branch Code"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={Branchcodetemplate}
              filter
              filterPlaceholder="Search by location"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Action"
              style={{ minWidth: "12rem" }}
              body={verifiedBodyTemplate}
            />
          </DataTable>
          <Sidebar
            visible={visibleRight}
            onHide={() => setVisibleRight(false)}
            baseZIndex={1000}
            position="right"
          >
            <h4 style={{ fontWeight: "normal" }}>Location Information</h4>
            <div className="mt-5 p-fluid">
              <div className="field">
                <label htmlFor="code">Location Code</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.location_code}
                  onChange={(e) => setLocationCode1(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Location Name</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.location_name}
                  onChange={(e) => setlocationname(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Location Description</label>
                <InputTextarea
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.location_desc}
                  onChange={(e) => setlocationdesc(e.target.value)}
                  disabled={show == "read" ? true : false}
                  autoResize
                  rows="3"
                  cols="30"
                />
              </div>
              <div className="field">
                <label htmlFor="name">Location Adress</label>
                <InputTextarea
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.location_address}
                  onChange={(e) => setlocationadress(e.target.value)}
                  disabled={show == "read" ? true : false}
                  autoResize
                  rows="3"
                  cols="30"
                />
              </div>

              <div className="field">
                <label htmlFor="name">Branch Code</label>
                <Dropdown
                  value={branchCode}
                  onChange={(e) => setBranchCode(e.value)}
                  options={items2}
                  optionLabel="name"
                  // placeholder={detail.location_name}
                  disabled={show == "read" ? true : false}
                />
              </div>

              <div className="field">
                <label htmlFor="name">City</label>
                <Dropdown
                  value={citycode}
                  onChange={(e) => setcitycode(e.value)}
                  options={items}
                  optionLabel="name"
                  // placeholder={detail.location_name}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Post Code</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.post_code}
                  onChange={(e) => setpostcode(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Phone Number</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.phone_number}
                  onChange={(e) => setphonenumber(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              {show == "update" || show == "post" ? (
                <Button
                  className="mt-5"
                  label="Save"
                  severity="Info"
                  onClick={show == "post" ? SubmitCountryData : editCountryData}
                />
              ) : (
                <></>
              )}
            </div>
          </Sidebar>
        </div>
      </div>
    </div>
  );
};

export default Location;
