import React, { useState, useEffect } from "react";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
// api
import { postData, getData } from "../../api";
import { Sidebar } from "primereact/sidebar";
import { Messaege } from "../../utilities/helper";
import { ConfirmPopup, confirmPopup } from "primereact/confirmpopup";
import { useRef } from "react";
import { Toast } from "primereact/toast";
import { Dropdown } from "primereact/dropdown";

const ContactUser = () => {
  const [filters1, setFilters1] = useState(null);
  const [loading1, setLoading1] = useState(true);
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [dataCountry, setDataCountry] = useState([]);
  const [visibleRight, setVisibleRight] = useState(false);
  const [detail, setDetail] = useState({});
  const [contryCode, setCountryCode] = useState("");
  const [contryName, setCountryName] = useState("");
  const [phone, setPhone] = useState("");
  const [mailuser, setMailuser] = useState("");
  const [contact_address, setcontact_address] = useState("");
  const [show, setShow] = useState("");
  const toast = useRef(null);
  const [dataCodeContry, setDataCodeCountry] = useState([]);
  const [codeCountry, setCodeCountry] = useState("");
  const [dataPrevCode, setDataPrevCode] = useState([]);
  const [prevcode, setPrevcode] = useState("");
  const [dataCity, setDataCity] = useState([]);
  const [city, setCity] = useState("");
  const [dataContactCategory, setDataContactCategory] = useState([]);
  const [contactCategory, setContactCategory] = useState("");
  const [dataRefeal, setDataRefeal] = useState([]);
  const [refeal, setRefeal] = useState("");

  const onGlobalFilterChange1 = (e) => {
    const value = e.target.value;
    let _filters1 = { ...filters1 };
    _filters1["global"].value = value;

    setFilters1(_filters1);
    setGlobalFilterValue1(value);
  };

  const renderHeader1 = () => {
    return (
      <div className="flex justify-content-between">
        <Button
          type="button"
          // icon="pi pi-filter-slash"
          label="Add Data"
          outlined
          onClick={() => {
            setVisibleRight(true);
            setShow("post");
          }}
        />
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue1}
            onChange={onGlobalFilterChange1}
            placeholder="Keyword Search"
          />
        </span>
      </div>
    );
  };

  const contactData = async () => {
    try {
      const response = await getData("/contact/read?_=1686045944126");
      var temp = [];
      temp = response.data.data.original.data;
      console.log(temp);
      setDataCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    contactData();
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      name: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      contact_name: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      representative: { value: null, matchMode: FilterMatchMode.IN },
      date: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.DATE_IS }],
      },
      balance: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      status: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      activity: { value: null, matchMode: FilterMatchMode.BETWEEN },
      verified: { value: null, matchMode: FilterMatchMode.EQUALS },
    });
    setGlobalFilterValue1("");
  };

  const contactBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.contact_code}
        </span>
      </React.Fragment>
    );
  };

  const nameBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.contact_name}
        </span>
      </React.Fragment>
    );
  };

  const phoneTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.phone}
        </span>
      </React.Fragment>
    );
  };

  const mailTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.mail}
        </span>
      </React.Fragment>
    );
  };
  const codeTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.ccategory_code}
        </span>
      </React.Fragment>
    );
  };

  const filterClearTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-times"
        onClick={options.filterClearCallback}
        severity="secondary"
      ></Button>
    );
  };

  const filterApplyTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-check"
        onClick={options.filterApplyCallback}
        severity="success"
      ></Button>
    );
  };

  const getcontactData = async (code) => {
    try {
      const response = await getData(`/contact/datamodal?contact_code=${code}`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDetail(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const SubmitCountryData = async () => {
    try {
      const response = await postData("/contact/submit", {
        contact_code: contryCode,
        contact_name: contryName,
        contact_address: contact_address,
        ccategory_code: contactCategory,
        ref_code: refeal,
        country_code: codeCountry,
        prev_code: prevcode,
        city_code: city,
        phone: phone,
        mail: mailuser,
        user: "1",
      });
      console.log(response);
      if (response.data.code == 300) {
        Messaege("Failed", "Make sure everything is filled", "error");
      } else {
        Messaege("Succes", "Succes add data", "success");
      }
      setVisibleRight(false);
      contactData();
    } catch (error) {
      console.log(error);
    }
  };

  const editCountryData = async () => {
    try {
      const response = await postData("/contact/save", {
        contact_code: contryCode ? contryCode : detail.contact_code,
        contact_name: contryName ? contryName : detail.contact_name,
        contact_address: contact_address
          ? contact_address
          : detail.contact_address,
        ccategory_code: contactCategory
          ? contactCategory
          : detail.ccategory_code,
        ref_code: refeal ? refeal : detail.ref_code,
        country_code: codeCountry ? codeCountry : detail.contact_code,
        prev_code: prevcode ? prevcode : detail.prev_code,
        city_code: city ? city : detail.city_code,
        phone: phone ? phone : detail.phone,
        mail: mailuser ? mailuser : detail.mail,
        user: detail.user ? detail.user : "1",
      });
      console.log(response);
      if (response.data.code == 300) {
        Messaege("Failed", "Make sure everything is filled", "error");
      } else {
        Messaege("Succes", "Succes add data", "success");
      }
      setVisibleRight(false);
      contactData();
    } catch (error) {
      console.log(error);
    }
  };

  const verifiedBodyTemplate = (rowData) => {
    const delcontactData = async () => {
      try {
        const response = await postData("/contact/delete", {
          contact_code: rowData.contact_code,
          contact_name: rowData.contact_name,
          contact_address: rowData.contact_address,
          ccategory_code: rowData.ccategory_code,
          ref_code: rowData.ref_code,
          country_code: rowData.country_code,
          prev_code: rowData.prev_code,
          city_code: rowData.contact_code,
          phone: rowData.phone,
          mail: rowData.mail,
          user: rowData.user ? rowData.user : "1",
        });
        console.log(response);
        Messaege("Succes", "Succes delete data", "success");
        contactData();
      } catch (error) {
        console.log(error);
      }
    };
    const accept = () => {
      toast.current.show({
        severity: "info",
        summary: "Confirmed",
        detail: "You have accepted",
        life: 3000,
      });
      delcontactData();
    };

    const reject = () => {
      toast.current.show({
        severity: "error",
        summary: "Rejected",
        detail: "You have rejected",
        life: 3000,
      });
    };

    const confirm = (event) => {
      confirmPopup({
        target: event.currentTarget,
        message: "Are you sure you want to delete data?",
        icon: "pi pi-exclamation-triangle",
        accept,
        reject,
      });
    };

    return (
      <>
        <div className="flex mx-3 gap-5">
          <Button
            type="button"
            icon="pi text-white pi-book"
            severity="success"
            onClick={() => {
              setVisibleRight(true);
              getcontactData(rowData.contact_code);
              setShow("read");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            type="button"
            icon="pi text-white pi-pencil"
            severity="warning"
            onClick={() => {
              setVisibleRight(true), setShow("update");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            onClick={confirm}
            type="button"
            icon="pi text-white pi-trash"
            severity="danger"
            style={{ marginRight: ".25em" }}
          ></Button>
        </div>
      </>
    );
  };

  const header1 = renderHeader1();

  const getprevDatacountry = async () => {
    try {
      const response = await getData(`/contact/countryloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataCodeCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getDataPrev = async () => {
    try {
      const response = await getData(`/contact/prevloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataPrevCode(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getDataCity = async () => {
    try {
      const response = await getData(`/contact/cityloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataCity(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getContactCategory = async () => {
    try {
      const response = await getData(`/contact/ccategoryloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataContactCategory(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getDataRefeal = async () => {
    try {
      const response = await getData(`/contact/refcodeloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataRefeal(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getprevDatacountry();
    getDataPrev();
    getDataCity();
    getContactCategory();
    getDataRefeal();
  }, []);

  const items = dataCodeContry?.map((item) => {
    const data = {};
    data.name = item.country_name;
    data.value = item.country_code;
    return data;
  });

  const items2 = dataPrevCode?.map((item) => {
    const data = {};
    data.name = item.prev_name;
    data.value = item.prev_code;
    return data;
  });

  const items3 = dataCity?.map((item) => {
    const data = {};
    data.name = item.city_name;
    data.value = item.city_code;
    return data;
  });

  const items4 = dataContactCategory?.map((item) => {
    const data = {};
    data.name = item.ccategory_name;
    data.value = item.ccategory_code;
    return data;
  });

  const items5 = dataRefeal?.map((item) => {
    const data = {};
    data.name = item.contact_name;
    data.value = item.contact_code;
    return data;
  });

  return (
    <div className="grid">
      <Toast ref={toast} />
      <ConfirmPopup />
      <div className="col-12">
        <div className="card">
          <h5>Filter Menu</h5>
          <DataTable
            value={dataCountry}
            paginator
            className="p-datatable-gridlines"
            showGridlines
            rows={10}
            dataKey="id"
            filters={filters1}
            filterDisplay="menu"
            loading={loading1}
            responsiveLayout="scroll"
            emptyMessage="No customers found."
            header={header1}
          >
            <Column
              header="Code"
              filterField="contact"
              style={{ minWidth: "12rem" }}
              body={contactBodyTemplate}
              filter
              filterPlaceholder="Search by contact"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Category"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={codeTemplate}
              filter
              filterPlaceholder="Search by contact"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Name"
              filterField="contact_name"
              style={{ minWidth: "12rem" }}
              body={nameBodyTemplate}
              filter
              filterPlaceholder="Search by contact"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Phone"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={phoneTemplate}
              filter
              filterPlaceholder="Search by contact"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Mail"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={mailTemplate}
              filter
              filterPlaceholder="Search by contact"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Action"
              style={{ minWidth: "12rem" }}
              body={verifiedBodyTemplate}
            />
          </DataTable>
          <Sidebar
            visible={visibleRight}
            onHide={() => setVisibleRight(false)}
            baseZIndex={1000}
            position="right"
          >
            <h4 style={{ fontWeight: "normal" }}>Contact Information</h4>
            <div className="mt-5 p-fluid">
              <div className="field">
                <label htmlFor="code">Code</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.contact_code}
                  onChange={(e) => setCountryCode(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Name</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.contact_name}
                  onChange={(e) => setCountryName(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Phone</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.phone}
                  onChange={(e) => setCountryName(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Mail</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.mail}
                  onChange={(e) => setCountryName(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Adress</label>
                <InputTextarea
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.contact_address}
                  onChange={(e) => setCountryName(e.target.value)}
                  disabled={show == "read" ? true : false}
                  autoResize
                  rows="3"
                  cols="30"
                />
              </div>
              <div className="field">
                <label htmlFor="name">Country</label>
                <Dropdown
                  value={codeCountry}
                  onChange={(e) => setCodeCountry(e.value)}
                  options={items}
                  optionLabel="name"
                  placeholder={show == "post" ? "" : detail.country_code}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Prev</label>
                <Dropdown
                  value={prevcode}
                  onChange={(e) => setPrevcode(e.value)}
                  options={items2}
                  optionLabel="name"
                  placeholder={show == "post" ? "" : detail.prev_code}
                />
              </div>
              <div className="field">
                <label htmlFor="name">City</label>
                <Dropdown
                  value={city}
                  onChange={(e) => setCity(e.value)}
                  options={items3}
                  optionLabel="name"
                  placeholder={show == "post" ? "" : detail.city_code}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Contact Category</label>
                <Dropdown
                  value={contactCategory}
                  onChange={(e) => setContactCategory(e.value)}
                  options={items4}
                  optionLabel="name"
                  placeholder={show == "post" ? "" : detail.ccategory_code}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Referral Code</label>
                <Dropdown
                  value={refeal}
                  onChange={(e) => setRefeal(e.value)}
                  options={items5}
                  optionLabel="name"
                  placeholder={show == "post" ? "" : detail.ref_code}
                />
              </div>
              {show == "update" || show == "post" ? (
                <Button
                  className="mt-5"
                  label="Save"
                  severity="Info"
                  onClick={show == "post" ? SubmitCountryData : editCountryData}
                />
              ) : (
                <></>
              )}
            </div>
          </Sidebar>
        </div>
      </div>
    </div>
  );
};

export default ContactUser;
