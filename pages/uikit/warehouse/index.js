import React, { useState, useEffect } from "react";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
// api
import { postData, getData } from "../../api";
import { Sidebar } from "primereact/sidebar";
import { Messaege } from "../../utilities/helper";
import { ConfirmPopup, confirmPopup } from "primereact/confirmpopup";
import { useRef } from "react";
import { Toast } from "primereact/toast";
import { Dropdown } from "primereact/dropdown";
import { InputTextarea } from "primereact/inputtextarea";

const Warehouse = () => {
  const [filters1, setFilters1] = useState(null);
  const [loading1, setLoading1] = useState(true);
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [dataCountry, setDataCountry] = useState([]);
  const [visibleRight, setVisibleRight] = useState(false);
  const [detail, setDetail] = useState({});
  const [dataCodeContry, setDataCodeCountry] = useState([]);
  const [show, setShow] = useState("");
  const toast = useRef(null);
  const [warehouseCode1, setWarehouseCode1] = useState("");
  const [warehousename, setwarehousename] = useState("");
  const [warehousedesc, setwarehousedesc] = useState("");
  const [warehouseadress, setwarehouseadress] = useState("");
  const [citycode, setcitycode] = useState("");
  const [postcode, setpostcode] = useState("");
  const [phonenumber, setphonenumber] = useState("");
  const [branchCode, setBranchCode] = useState("");
  const [locationCode, setLocationCode] = useState("");
  const [dataBranchcode, setdataBranchCode] = useState([]);
  const [locationloop, setLocationLoop] = useState([]);

  const onGlobalFilterChange1 = (e) => {
    const value = e.target.value;
    let _filters1 = { ...filters1 };
    _filters1["global"].value = value;

    setFilters1(_filters1);
    setGlobalFilterValue1(value);
  };

  const renderHeader1 = () => {
    return (
      <div className="flex justify-content-between">
        <Button
          type="button"
          // icon="pi pi-filter-slash"
          label="Add Data"
          outlined
          onClick={() => {
            setVisibleRight(true);
            setShow("post");
          }}
        />
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue1}
            onChange={onGlobalFilterChange1}
            placeholder="Keyword Search"
          />
        </span>
      </div>
    );
  };

  const warehouseData = async () => {
    try {
      const response = await getData("/warehouse/read?_=1686045944126");
      var temp = [];
      temp = response.data.data.original.data;
      console.log(temp);
      setDataCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    warehouseData();
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      name: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      "warehouse_name": {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      representative: { value: null, matchMode: FilterMatchMode.IN },
      date: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.DATE_IS }],
      },
      balance: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      status: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      activity: { value: null, matchMode: FilterMatchMode.BETWEEN },
      verified: { value: null, matchMode: FilterMatchMode.EQUALS },
    });
    setGlobalFilterValue1("");
  };

  const warehouseBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.warehouse_code}
        </span>
      </React.Fragment>
    );
  };

  const nameBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.warehouse_name}
        </span>
      </React.Fragment>
    );
  };

  const Branchcodetemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.division_code}
        </span>
      </React.Fragment>
    );
  };

  const Locationcodetemplate = (rowData) => {
    return (
      <React.Fragment>
        <span style={{ marginLeft: ".5em", verticalAlign: "middle" }}>
          {rowData.location_code}
        </span>
      </React.Fragment>
    );
  };

  const filterClearTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-times"
        onClick={options.filterClearCallback}
        severity="secondary"
      ></Button>
    );
  };

  const filterApplyTemplate = (options) => {
    return (
      <Button
        type="button"
        icon="pi pi-check"
        onClick={options.filterApplyCallback}
        severity="success"
      ></Button>
    );
  };

  const getwarehouseData = async (code) => {
    try {
      const response = await getData(
        `/warehouse/datamodal?warehouse_code=${code}`
      );
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDetail(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const SubmitCountryData = async () => {
    try {
      const response = await postData("/warehouse/submit", {
        warehouse_code: warehouseCode1,
        warehouse_name: warehousename,
        warehouse_address: warehouseadress,
        division_code: branchCode,
        location_code: locationCode,
        city_code: citycode,
        post_code: postcode,
        phone_number: phonenumber,
        user: "1",
      });
      console.log(response);
      if (response.data.code == 300) {
        Messaege("Failed", "Make sure everything is filled", "error");
      } else {
        Messaege("Succes", "Succes add data", "success");
      }
      setVisibleRight(false);
      warehouseData();
    } catch (error) {
      console.log(error);
    }
  };

  const editCountryData = async () => {
    try {
      const response = await postData("/warehouse/save", {
        warehouse_code: warehouseCode1 ? warehouseCode1 : detail.warehouse_code,
        warehouse_name: warehousename ? warehousename : detail.warehouse_name,
        warehouse_desc: warehousedesc ? warehousedesc : detail.warehouse_desc,
        warehouse_address: warehouseadress
          ? warehouseadress
          : detail.warehouse_address,
        division_code: branchCode ? branchCode : detail.branch_code,
        location_code: locationCode ? locationCode : detail.location_code,
        city_code: citycode ? citycode : detail.city_code,
        post_code: postcode ? postcode : detail.post_code,
        phone_number: phonenumber ? phonenumber : detail.phone_number,
        user: detail.user ? detail.user : "1",
      });
      console.log(response);
      if (response.data.code == 300) {
        Messaege("Failed", "Make sure everything is filled", "error");
      } else {
        Messaege("Succes", "Succes add data", "success");
      }
      setVisibleRight(false);
      warehouseData();
    } catch (error) {
      console.log(error);
    }
  };

  const verifiedBodyTemplate = (rowData) => {
    const delwarehouseData = async () => {
      try {
        const response = await postData("/warehouse/delete", {
          warehouse_code: rowData.warehouse_code,
          warehouse_name: rowData.warehouse_name,
          warehouse_desc: rowData.warehouse_desc,
          warehouse_address: rowData.warehouse_address,
          division_code: rowData.division_code,
          location_code: rowData.location_code,
          city_code: rowData.city_code,
          post_code: rowData.post_code,
          phone_number: rowData.phone_number,
          user: rowData.user ? rowData.user : "1",
        });
        console.log(response);
        Messaege("Succes", "Succes delete data", "success");
        warehouseData();
      } catch (error) {
        console.log(error);
      }
    };
    const accept = () => {
      toast.current.show({
        severity: "info",
        summary: "Confirmed",
        detail: "You have accepted",
        life: 3000,
      });
      delwarehouseData();
    };

    const reject = () => {
      toast.current.show({
        severity: "error",
        summary: "Rejected",
        detail: "You have rejected",
        life: 3000,
      });
    };

    const confirm = (event) => {
      confirmPopup({
        target: event.currentTarget,
        message: "Are you sure you want to delete data?",
        icon: "pi pi-exclamation-triangle",
        accept,
        reject,
      });
    };

    return (
      <>
        <div className="flex mx-3 gap-5">
          <Button
            type="button"
            icon="pi text-white pi-book"
            severity="success"
            onClick={() => {
              setVisibleRight(true);
              getwarehouseData(rowData.warehouse_code);
              setShow("read");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            type="button"
            icon="pi text-white pi-pencil"
            severity="warning"
            onClick={() => {
              setVisibleRight(true), setShow("update");
            }}
            style={{ marginRight: ".25em" }}
          />
          <Button
            onClick={confirm}
            type="button"
            icon="pi text-white pi-trash"
            severity="danger"
            style={{ marginRight: ".25em" }}
          ></Button>
        </div>
      </>
    );
  };

  const header1 = renderHeader1();

  const getwarehouseDatacountry = async (code) => {
    try {
      const response = await getData(`/warehouse/cityloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setDataCodeCountry(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getBranchCode = async (code) => {
    try {
      const response = await getData(`/warehouse/divisionloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setdataBranchCode(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getLocation = async (code) => {
    try {
      const response = await getData(`/warehouse/locationloop`);
      var temp = [];
      temp = response.data;
      console.log(temp);
      setLocationLoop(temp);
      setLoading1(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getwarehouseDatacountry();
    getBranchCode();
    getLocation();
  }, []);

  const items = dataCodeContry?.map((item) => {
    const data = {};
    data.name = item.city_name;
    data.value = item.city_code;
    return data;
  });

  const items2 = dataBranchcode?.map((item) => {
    const data = {};
    data.name = item.division_name;
    data.value = item.division_code;
    return data;
  });

  const items3 = locationloop?.map((item) => {
    const data = {};
    data.name = item.location_name;
    data.value = item.location_code;
    return data;
  });

  return (
    <div className="grid">
      <Toast ref={toast} />
      <ConfirmPopup />
      <div className="col-12">
        <div className="card">
          <h5>Filter Menu</h5>
          <DataTable
            value={dataCountry}
            paginator
            className="p-datatable-gridlines"
            showGridlines
            rows={10}
            dataKey="id"
            filters={filters1}
            filterDisplay="menu"
            loading={loading1}
            responsiveLayout="scroll"
            emptyMessage="No customers found."
            header={header1}
          >
            <Column
              header="Warehouse Code"
              filterField="warehouse"
              style={{ minWidth: "12rem" }}
              body={warehouseBodyTemplate}
              filter
              filterPlaceholder="Search by warehouse"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Warehouse Name"
              filterField="warehouse_name"
              style={{ minWidth: "12rem" }}
              body={nameBodyTemplate}
              filter
              filterPlaceholder="Search by warehouse"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Division Code"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={Branchcodetemplate}
              filter
              filterPlaceholder="Search by warehouse"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Location Code"
              filterField="name"
              style={{ minWidth: "12rem" }}
              body={Locationcodetemplate}
              filter
              filterPlaceholder="Search by warehouse"
              filterClear={filterClearTemplate}
              filterApply={filterApplyTemplate}
            />
            <Column
              header="Action"
              style={{ minWidth: "12rem" }}
              body={verifiedBodyTemplate}
            />
          </DataTable>
          <Sidebar
            visible={visibleRight}
            onHide={() => setVisibleRight(false)}
            baseZIndex={1000}
            position="right"
          >
            <h4 style={{ fontWeight: "normal" }}>Warehouse Information</h4>
            <div className="mt-5 p-fluid">
              <div className="field">
                <label htmlFor="code">Warehouse Code</label>
                <InputText
                  id="code"
                  type="text"
                  placeholder={show == "post" ? "" : detail.warehouse_code}
                  onChange={(e) => setWarehouseCode1(e.target.value)}
                  disabled={show == "update" || show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Warehouse Name</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.warehouse_name}
                  onChange={(e) => setwarehousename(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Warehouse Adress</label>
                <InputTextarea
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.warehouse_address}
                  onChange={(e) => setwarehouseadress(e.target.value)}
                  disabled={show == "read" ? true : false}
                  autoResize
                  rows="3"
                  cols="30"
                />
              </div>

              <div className="field">
                <label htmlFor="name">Division</label>
                <Dropdown
                  value={branchCode}
                  onChange={(e) => setBranchCode(e.value)}
                  options={items2}
                  optionLabel="name"
                  // placeholder={detail.warehouse_name}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Location</label>
                <Dropdown
                  value={locationCode}
                  onChange={(e) => setLocationCode(e.value)}
                  options={items3}
                  optionLabel="name"
                  // placeholder={detail.warehouse_name}
                  disabled={show == "read" ? true : false}
                />
              </div>

              <div className="field">
                <label htmlFor="name">City</label>
                <Dropdown
                  value={citycode}
                  onChange={(e) => setcitycode(e.value)}
                  options={items}
                  optionLabel="name"
                  // placeholder={detail.warehouse_name}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Post Code</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.post_code}
                  onChange={(e) => setpostcode(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              <div className="field">
                <label htmlFor="name">Phone Number</label>
                <InputText
                  id="name"
                  type="text"
                  placeholder={show == "post" ? "" : detail.phone_number}
                  onChange={(e) => setphonenumber(e.target.value)}
                  disabled={show == "read" ? true : false}
                />
              </div>
              {show == "update" || show == "post" ? (
                <Button
                  className="mt-5"
                  label="Save"
                  severity="Info"
                  onClick={show == "post" ? SubmitCountryData : editCountryData}
                />
              ) : (
                <></>
              )}
            </div>
          </Sidebar>
        </div>
      </div>
    </div>
  );
};

export default Warehouse;
