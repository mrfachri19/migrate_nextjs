import axios from "axios";
import CONFIG from "../config";
import { enc, AES } from "crypto-js";
import { getToken } from "../utilities/storage";

const fullURL = (path) => {
  return `${CONFIG.API_URL}/${path}`;
};

export const handleNetworkError = (error) => {
  if (error.message === "Network request failed") {
    alert(
      "Kesalahan Jaringan",
      "Silakan periksa koneksi Anda dan coba kembali.",
      "iconNoInet"
    );
  }
  throw error;
};

const post =
  (api) =>
  async (param = "", data) => {
    try {
      const decryptedBytes = AES.decrypt(getToken(), "Rahasia123");
      const decryptedToken = decryptedBytes.toString(enc.Utf8);
      return await axios.post(
        `${fullURL(api)}${param}`,
        data,
        {
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            Authorization: `Bearer ${
             getToken()
                ? decryptedToken
                : (window.location.href = "/auth/login")
            }`,
          },
        },
        { handleNetworkError }
      );
    } catch (err) {
      console.log(err);
    }
  };

const postLogin = (api) => (data) => {
  return axios.post(fullURL(api), data, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "X-Authorization": `Basic ZGlhcml1bV83MzI4NzA1OTQ6WGN2Y3pFM1RkWFFQ`,
    },
  });
};

const patch =
  (api) =>
  async (param = "", data) => {
    try {
      return await axios.patch(
        `${fullURL(api)}${param}`,
        data,
        {
          method: "PATCH",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            Authorization: `Bearer ${
              getToken()
                ? getToken()
                : (window.location.href = "/auth/login")
            }`,
          },
        },
        { handleNetworkError }
      );
    } catch (err) {
      console.log(err);
    }
  };

const del = (api) => (data) => {
  return axios.delete(fullURL(api), data, {
    method: "DELETE",
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-type": "application/json",
      Authorization: `Bearer ${
        getToken()
          ? getToken()
          : (window.location.href = "/auth/login")
      }`,
    },
  });
};

const get =
  (api) =>
  async (param = "") => {
    try {
      const decryptedBytes = AES.decrypt(getToken(), "Rahasia123");
      const decryptedToken = decryptedBytes.toString(enc.Utf8);
      return await axios(
        `${fullURL(api)}${param}`,
        {
          method: "GET",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            Authorization: `Bearer ${
              getToken()
                ? decryptedToken
                : (window.location.href = "/auth/login")
            }`,
          },
        },
        { handleNetworkError }
      );
    } catch (err) {
      console.log(err);
    }
  };

//auth
export const loginUser = postLogin("api/apilogin");

export const getData = get("api");
export const postData = post("api");

const API = {
  // country
  getData,
  postData,
};

export default API;
